// Import jQuery
//=require ../../../node_modules/jquery/dist/jquery.js

// Import Popper
//=require ../../../node_modules/popper.js/dist/umd/popper.js

// Import jQuery.maskedinput
//=require ../../../node_modules/jquery.maskedinput/src/jquery.maskedinput.js

// Import Slick
//=require ../../../node_modules/slick-carousel/slick/slick.js

// Import jQuery-UI
//=require ../../../node_modules/jquery-ui/ui/version.js
//=require ../../../node_modules/jquery-ui/ui/widget.js
//=require ../../../node_modules/jquery-ui/ui/position.js
//=require ../../../node_modules/jquery-ui/ui/keycode.js
//=require ../../../node_modules/jquery-ui/ui/unique-id.js
//=require ../../../node_modules/jquery-ui/ui/labels.js
//=require ../../../node_modules/jquery-ui/ui/widgets/autocomplete.js
//=require ../../../node_modules/jquery-ui/ui/widgets/datepicker.js
//=require ../../../node_modules/jquery-ui/ui/widgets/menu.js
//=require ../../../node_modules/jquery-ui/ui/escape-selector.js
//=require ../../../node_modules/jquery-ui/ui/form-reset-mixin.js
//=require ../../../node_modules/jquery-ui/ui/widgets/selectmenu.js
//=require ../../../node_modules/jquery-ui/ui/i18n/datepicker-ru.js


// Import jQuery.ui.touch-punch
//=require ../../../node_modules/jquery-ui-touch-punch/jquery.ui.touch-punch.js

// Import Bootstrap 4
//=require ../../../node_modules/bootstrap/js/dist/util.js
//=require ../../../node_modules/bootstrap/js/dist/alert.js
//=require ../../../node_modules/bootstrap/js/dist/button.js
//=require ../../../node_modules/bootstrap/js/dist/carousel.js
//=require ../../../node_modules/bootstrap/js/dist/collapse.js
//=require ../../../node_modules/bootstrap/js/dist/dropdown.js
//=require ../../../node_modules/bootstrap/js/dist/modal.js
//=require ../../../node_modules/bootstrap/js/dist/scrollspy.js
//=require ../../../node_modules/bootstrap/js/dist/tab.js
//=require ../../../node_modules/bootstrap/js/dist/tooltip.js
//=require ../../../node_modules/bootstrap/js/dist/popover.js

//=require '../../../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js'