'use strict';

$(function () {
	$('.fancybox').fancybox();
	$('[name="phone"]').mask("+7 (999) 999-99-99");

	var $y2019 = $('.slick__2019');
	var $y2018 = $('.slick__2018');
	var $y2017 = $('.slick__2017');
	var $y2016 = $('.slick__2016');
	var $y2015 = $('.slick__2015');
	var $y2014 = $('.slick__2014');
	var $y2013 = $('.slick__2013');
	var $y2012 = $('.slick__2012');
	var $y2011 = $('.slick__2011');
	var $y2010 = $('.slick__2010');
	var $y2009 = $('.slick__2009');

	var $pStatus = $('.personal__count');
	var $pSlickElement = $('.slick__personal');

	$pSlickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
		var i = (currentSlide ? currentSlide : 0) + 1;
		$pStatus.text('0' + i + ' \u2013 0' + slick.slideCount);
	});

	$pSlickElement.slick({
		touchToSwipe: true,
		nextArrow: $('.personal__next'),
		prevArrow: $('.personal__prev'),
		centerMode: true,
		slidesToShow: 3,
		centerPadding: 0,
		responsive: [{
			breakpoint: 1700,
			settings: {
				slidesToShow: 2
			}
		}, {
			breakpoint: 1450,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 1350,
			settings: {
				slidesToShow: 2
			}
		}, {
			breakpoint: 1200,
			settings: {
				centerPadding: '15px',
				slidesToShow: 2
			}
		}, {
			breakpoint: 385,
			settings: {
				// slidesToShow: 1,
				centerPadding: '0px',
				slidesToShow: 1
			}
		}]
	});

	$('.slick__dot').slick({
		slidesToShow: 1,
		touchToSwipe: true,
		centerMode: true,
		arrows: false,
		centerPadding: '25%',
		responsive: [{
			breakpoint: 991,
			settings: {
				slidesToShow: 1,
				centerPadding: '50px'
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				centerPadding: '35px'
			}
		}]
	});

	$('.slick__history').slick({
		slidesToShow: 1,
		touchToSwipe: true,
		arrows: false,
		dots: true,
		appendDots: $('.history__dots')
	});

	$('.slick__header').slick({
		slidesToShow: 1,
		dots: true,
		arrows: true,
		fade: true,
		appendDots: $('.slick-dots-header'),
		prevArrow: $('.slick__header-pagination').find('.prev'),
		nextArrow: $('.slick__header-pagination').find('.next'),
		responsive: [{
			breakpoint: 768,
			settings: {
				adaptiveHeight: true
			}
		}]
	});

	var $projectCount = $('.project-pagination .count');
	var $projectSlick = $('.project-slider');

	$projectSlick.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
		var i = (currentSlide ? currentSlide : 0) + 1;
		$projectCount.text(i + ' / ' + slick.slideCount);
	});

	$projectSlick.slick({
		slidesToShow: 3,
		arrows: true,
		prevArrow: $('.project-pagination').find('.prev'),
		nextArrow: $('.project-pagination').find('.next'),
		responsive: [{
			breakpoint: 1200,
			settings: {
				slidesToShow: 2
			}
		}, {
			breakpoint: 767,
			settings: {
				slidesToShow: 1
			}
		}]
	});

	var $blogCount = $('.blog-pagination .count');
	var $blogSlick = $('.blog-slider');

	$blogSlick.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
		var i = (currentSlide ? currentSlide : 0) + 1;
		$blogCount.text(i + ' / ' + slick.slideCount);
	});

	$blogSlick.slick({
		slidesToShow: 3,
		arrows: true,
		prevArrow: $('.blog-pagination').find('.prev'),
		nextArrow: $('.blog-pagination').find('.next'),
		responsive: [{
			breakpoint: 1200,
			settings: {
				slidesToShow: 2
			}
		}, {
			breakpoint: 767,
			settings: {
				slidesToShow: 1
			}
		}]
	});

	var $histSlick = $('.history-slider');

	$histSlick.slick({
		slidesToShow: 1,
		arrows: true,
		fade: true,
		prevArrow: $('.history-pagination').find('.prev'),
		nextArrow: $('.history-pagination').find('.next')
	});

	$('.history-main .dots').on('click', 'a', function () {
		$('.history-main .dots').find('a').removeClass('active');
		$(this).addClass('active'), $('.history-slider').slick('slickGoTo', $(this).data('id'));
		return false;
	});

	$histSlick.on('beforeChange', function (e, s, c, n) {
		$('.history-main .dots').find('a').removeClass('active');
		$('.history-main .dots').find('a[data-id="' + n + '"]').addClass('active');
	});

	$('.similar-slider').slick({
		slidesToShow: 3,
		arrows: false,
		responsive: [{
			breakpoint: 1200,
			settings: {
				slidesToShow: 2
			}
		}, {
			breakpoint: 767,
			settings: {
				slidesToShow: 1
			}
		}]
	});

	if ($(window).width() > 575) {
		$('.services__scroll').mCustomScrollbar();
	}

	$('.services__title').on('click', function () {
		$(this).parents('.services__items').toggleClass('active');
		$(this).next('.services__body').slideToggle('fast');
		return false;
	});

	$('.services__items.active').each(function () {
		$(this).find('.services__body').slideToggle('fast');
	});

	$('.faq__title').on('click', function () {
		$(this).parents('.faq__items').toggleClass('active');
		$(this).next('.faq__body').slideToggle('fast');
		return false;
	});

	$('.faq__items.active').each(function () {
		$(this).find('.faq__body').slideToggle('fast');
	});

	$('.work__items').hover(function () {
		var $title = $(this).find('.text').text();
		var $text = $(this).data('text');
		var $img = $(this).find('img').attr('src');

		$('.work__hovered').show().find('strong').html($title);
		$('.work__hovered').find('p').html($text);
		$('.work__hovered').find('img').attr('src', $img);
		$('.work__title').hide();
	}, function () {
		$('.work__title').show();
		$('.work__hovered').hide();
	});

	$('.dot').on('click', '.d-flex', function () {
		$('.dot').removeClass('active');
		$(this).closest('.dot').addClass('active');
	});

	var elem = document.getElementById('masked');
	$('body').on('mousemove', function (e) {
		setMaskPos(e.clientX, e.clientY);
	});

	function setMaskPos(x, y) {
		$('#masked').css({
			'-webkit-mask-image': "radial-gradient(circle at " + x + "px " + y + "px, black 0px, black 70px, transparent 250px)"
		});
	}
	setMaskPos(-999, 0);

	$('#year').on('change', function (e) {
		$('#pills-tabContent').find('.tab-pane').removeClass('show active');
		$('.tab-pane#y' + e.target.value).addClass("show active");

		if (e.target.value == '2019' && !$('.slick__2019').hasClass('slick-slider')) {
			$y2019.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2019.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2019.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2019'),
				prevArrow: $('.p_prev-2019'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if (e.target.value == '2018' && !$('.slick__2018').hasClass('slick-slider')) {
			$y2018.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2018.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2018.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2018'),
				prevArrow: $('.p_prev-2018'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if (e.target.value == '2017' && !$('.slick__2017').hasClass('slick-slider')) {
			$y2017.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2017.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2017.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2017'),
				prevArrow: $('.p_prev-2017'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if (e.target.value == '2016' && !$('.slick__2016').hasClass('slick-slider')) {
			$y2016.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2016.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2016.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2016'),
				prevArrow: $('.p_prev-2016'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if (e.target.value == '2015' && !$('.slick__2015').hasClass('slick-slider')) {
			$y2015.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2015.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2015.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2015'),
				prevArrow: $('.p_prev-2015'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if (e.target.value == '2014' && !$('.slick__2014').hasClass('slick-slider')) {
			$y2014.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2014.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2014.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2014'),
				prevArrow: $('.p_prev-2014'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if (e.target.value == '2013' && !$('.slick__2013').hasClass('slick-slider')) {
			$y2013.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2013.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2013.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2013'),
				prevArrow: $('.p_prev-2013'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if (e.target.value == '2012' && !$('.slick__2012').hasClass('slick-slider')) {
			$y2012.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2012.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2012.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2012'),
				prevArrow: $('.p_prev-2012'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if (e.target.value == '2011' && !$('.slick__2011').hasClass('slick-slider')) {
			$y2011.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2011.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2011.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2011'),
				prevArrow: $('.p_prev-2011'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if (e.target.value == '2010' && !$('.slick__2010').hasClass('slick-slider')) {
			$y2010.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				arrows: false,
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if (e.target.value == '2009' && !$('.slick__2009').hasClass('slick-slider')) {
			$y2009.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				arrows: false,
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
	});

	$('.advantages').on('click', '[data-toggle="tab"]', function () {
		console.log($(this).attr('href'));
		$('#av-tab').find('.nav-link').removeClass('active');
		$('#av-content').find('.tab-pane').removeClass('show active');

		$('#av-tab').find('.nav-link[href="' + $(this).attr('href') + '"]').addClass('active');
		$('#av-content').find('.tab-pane' + $(this).attr('href')).addClass('show active');
		return false;
	});

	$('form').on('submit', function () {
		var _form = $(this);

		$.ajax({
			method: 'POST',
			data: _form.serialize(),
			url: './mail.php',
			beforeSend: function beforeSend() {
				_form.find('.btn').text('Отправка...');
			},
			success: function success(d) {
				_form.find('input').val('');
				_form.find('.btn').text('Отправить заявку');
				$('.modal').modal('hide');
				setTimeout(function () {
					$('#thanx').modal('show');
				}, 200);
			}
		});
		return false;
	});

	$(window).on('scroll', function () {
		if ($(window).scrollTop() > 200) {
			$('header .menu').addClass('sticky');
			$('header').addClass('sticky');
			$('.top_btn').addClass('show');
		} else {
			$('header .menu').removeClass('sticky');
			$('header').removeClass('sticky');
			$('.top_btn').removeClass('show');
		}
	});

	$('a.menu').on('click', function () {
		$('nav, .nav-overlay').show('fade', 200);
		return false;
	});
	$('nav .close').on('click', function () {
		$('nav, .nav-overlay').hide('fade', 200);
		return false;
	});
	$('nav ul a').on('click', function () {
		$('nav, .nav-overlay').hide('fade', 200);
	});

	$y2019.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
		var i = (currentSlide ? currentSlide : 0) + 1;
		$y2019.closest('.tab-pane').find('.project__count').each(function () {
			console.log(this);
			$(this).text('0' + i + ' / 0' + slick.slideCount);
		});
	});
	$y2019.slick({
		slidesToShow: 3,
		touchToSwipe: true,
		nextArrow: $('.project__next'),
		prevArrow: $('.project__prev'),
		responsive: [{
			breakpoint: 992,
			settings: {
				slidesToShow: 2
			}
		}, {
			breakpoint: 600,
			settings: {
				slidesToShow: 1
			}
		}]
	});

	$("a[data-toggle='tab']").on('shown.bs.tab', function (e, r) {

		if ($(e.target).attr('href') == '#y2018' && !$('.slick__2018').hasClass('slick-slider')) {
			$y2018.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2018.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2018.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2018'),
				prevArrow: $('.p_prev-2018'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if ($(e.target).attr('href') == '#y2017' && !$('.slick__2017').hasClass('slick-slider')) {
			$y2017.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2017.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2017.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2017'),
				prevArrow: $('.p_prev-2017'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if ($(e.target).attr('href') == '#y2016' && !$('.slick__2016').hasClass('slick-slider')) {
			$y2016.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2016.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2016.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2016'),
				prevArrow: $('.p_prev-2016'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if ($(e.target).attr('href') == '#y2015' && !$('.slick__2015').hasClass('slick-slider')) {
			$y2015.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2015.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2015.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2015'),
				prevArrow: $('.p_prev-2015'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if ($(e.target).attr('href') == '#y2014' && !$('.slick__2014').hasClass('slick-slider')) {
			$y2014.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2014.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2014.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2014'),
				prevArrow: $('.p_prev-2014'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if ($(e.target).attr('href') == '#y2013' && !$('.slick__2013').hasClass('slick-slider')) {
			$y2013.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2013.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2013.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2013'),
				prevArrow: $('.p_prev-2013'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if ($(e.target).attr('href') == '#y2012' && !$('.slick__2012').hasClass('slick-slider')) {
			$y2012.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2012.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2012.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2012'),
				prevArrow: $('.p_prev-2012'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if ($(e.target).attr('href') == '#y2011' && !$('.slick__2011').hasClass('slick-slider')) {
			$y2011.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
				var i = (currentSlide ? currentSlide : 0) + 1;
				$y2011.closest('.tab-pane').find('.project__count').text('0' + i + ' / 0' + slick.slideCount);
			});
			$y2011.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				nextArrow: $('.p_next-2011'),
				prevArrow: $('.p_prev-2011'),
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if ($(e.target).attr('href') == '#y2010' && !$('.slick__2010').hasClass('slick-slider')) {
			$y2010.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				arrows: false,
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
		if ($(e.target).attr('href') == '#y2009' && !$('.slick__2009').hasClass('slick-slider')) {
			$y2009.slick({
				slidesToShow: 3,
				touchToSwipe: true,
				arrows: false,
				responsive: [{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		}
	});

	$('.show-more').on('click', function () {
		$(this).prev('.spoiler').css({
			'height': 'auto'
		}).end().hide();

		return false;
	});

	$('a.spoiler').on('click', function () {
		$(this).nextAll('p').removeAttr('class');
		$(this).detach();
		return false;
	});

	// $('.history__masonry').masonry({
	// itemSelector: '.col-m',
	// gutter: 30
	// })

	$('#personal .scroll').mCustomScrollbar();
});
//# sourceMappingURL=app.js.map
