<?php

	if(isset($_POST)) {
		$phone = $_POST['phone'];		
		$comment = $_POST['comment'];
		$page = htmlspecialchars($_SERVER['HTTP_REFERER']);

		function mime_header_encode($str, $data_charset, $send_charset) {
		    if($data_charset != $send_charset)
		    $str = iconv($data_charset, $send_charset .'//IGNORE', $str);
		    return ('=?'. $send_charset .'?B?'. base64_encode($str) .'?=');
		}

		class TEmail {
		    public $from_email;
		    public $from_name;
		    public $to_email;
		    public $to_name;
		    public $subject;
		    public $data_charset = 'UTF-8';
		    public $send_charset = 'UTF-8';
		    public $body = '';
		    public $type = 'text/html';

		    function send() {
		        $dc = $this->data_charset;
		        $sc = $this->send_charset;
		        $enc_to = mime_header_encode($this->to_name, $dc, $sc) .' <'. $this->to_email .'>';
		        $enc_subject = mime_header_encode($this->subject, $dc, $sc);
		        $enc_from = mime_header_encode($this->from_name, $dc, $sc) .' <'.$this->from_email .'>';
		        $enc_body = $dc == $sc ? $this->body:iconv($dc, $sc .'//IGNORE', $this->body);
		        $headers = "";
		        $headers .= "Mime-Version: 1.0\r\n";
		        $headers .= "Content-type: ". $this->type ."; charset=". $sc ."\r\n";
		        $headers .= "From: ". $enc_from ."\r\n";
		        return mail($enc_to, $enc_subject, $enc_body, $headers);
		    }
		}

		$body = "Тема: Заявка<br/>";
		$body .= "Телефон: ".$phone."<br/>";
		$body .= "Комментарий: ".$comment."<br/>";

		$emailgo = new TEmail;
		$emailgo->from_email = "noreplay@chex.ru";
		$emailgo->to_email = "info@chex.ru";
		$emailgo->subject = $subject;
		$emailgo->body = $body;

		$emailgo2 = new TEmail;
		$emailgo2->from_email = "noreplay@chex.ru";
		$emailgo2->to_email = "tech@affetta.ru";
		$emailgo2->subject = $subject;
		$emailgo2->body = $body;

    $log_msg = PHP_EOL.'---------------['.date('d.m.Y H:i:s').']---------------'.PHP_EOL;
    $log_msg .= <<<MSG
Тип обращения: Заявка
Телефон: $phone
Комментарий: $comment
MSG;
		$log_msg .= PHP_EOL."Отправлено со страницы: ".$page ;
		$log_msg .= PHP_EOL.'---------------------------------------------------'.PHP_EOL;

		file_put_contents('mail_log.txt', $log_msg, FILE_APPEND);

		try {
		    $emailgo->send();
		    $emailgo2->send();
		    echo 1;
		} catch (Exception $e) {
		    echo $e;
		}
	}

?>